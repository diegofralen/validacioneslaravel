<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Pais;
use App\Ciudad;
use App\Categoria;
use App\Editorial;
use App\Autor;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        {
       
        
    	$this->call(CategoriaTableSeeder::class);
         $this->call(CiudadTableSeeder::class);  
          $this->call(UsuarioTableSeeder::class);
          $this->call(PaisTableSeeder::class);
          $this->call(EditorialesTableSeeder::class);
          }
    }
}
