<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Pais;
use App\Ciudad;
use App\tipolibro;
use App\Editorial;
use App\Autor;
use App\Categoria;

class CategoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('categorias')->delete();

        $today = date('Y-m-d H:i:s');

        $data = [
            ['name' => 'Antiguedades y Coleccionables'],
            ['name' => 'Arquitectura'],
            ['name' => 'Arte'],
            ['name' => 'Artes Escénicas'],
            ['name' => 'Autoayuda'],
            ['name' => 'Biografía y Autobiografía'],
            ['name' => 'Calendarios'],
            ['name' => 'Casa y Hogar'],
            ['name' => 'Ciencia'],
            ['name' => 'Ciencias Políticas'],
            ['name' => 'Ciencias Sociales'],
            ['name' => 'Cocina, Comida y Vinos'],
            ['name' => 'Colecciones Literarias'],
            ['name' => 'Comics y Novelas Gráficas'],
            ['name' => 'Computación e Internet'],
            ['name' => 'Crímenes Verdaderos'],
            ['name' => 'Crítica Literaria'],
            ['name' => 'Cuerpo, Mente y Espíritu'],
            ['name' => 'Deportes y Recreación'],
            ['name' => 'Drama '],
            ['name' => 'Educación'],
            ['name' => 'Estudio de Lenguas Extranjeras'],
            ['name' => 'Familia y Relaciones'],
            ['name' => 'Ficción'],
            ['name' => 'Ficción para Niños'],
            ['name' => 'Filosofía'],
            ['name' => 'Fotografía'],
            ['name' => 'Guías de Ayuda'],
            ['name' => 'Historia & Geografia'],
            ['name' => 'Humor '],
            ['name' => 'Jardinería'],
            ['name' => 'Juegos'],
            ['name' => 'Lengua y Literatura'],
            ['name' => 'Leyes'],
            ['name' => 'Manualidades y Hobbies'],
            ['name' => 'Mascotas y Animales'],
            ['name' => 'Matemáticas'],
            ['name' => 'Medicina'],
            ['name' => 'Música '],
            ['name' => 'Naturaleza y Aire libre'],
            ['name' => 'Negocios y Economia'],
            ['name' => 'Niños y Jóvenes'],
            ['name' => 'Papeleria '],
            ['name' => 'Poesía'],
            ['name' => 'Psicología'],
            ['name' => 'Referencia'],
            ['name' => 'Religión y Espiritualidad'],
            ['name' => 'Salud y Bienestar'],
            ['name' => 'Tecnología'],
            ['name' => 'Transporte'],
            ['name' => 'Tweens Fiction'],
            ['name' => 'Tweens Nonfiction'],
            ['name' => 'Viajes'],
            ['name' => 'Video y DVD'],
            ['name' => 'Ficcion juvenil'],
            ['name' => 'No Ficcion para adultos jovenes']
            
        ];

        foreach ($data as $categorias) {
            DB::table('categorias')->insert([
                'name' => $categorias['name'],
                'created_at' => $today,
                'updated_at' => $today
            ]);


    	}
	

    }
}
