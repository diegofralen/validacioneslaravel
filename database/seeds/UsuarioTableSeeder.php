<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Pais;
use App\Ciudad;
use App\tipolibro;
use App\Editorial;
use App\Autor;
use App\Libro;
class UsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          	$ciudads_creados=Ciudad::all(); 
         
         $usuarios =  [
        [
            'nombre' =>"Diego",
            'email' =>"diegoestudiante34@hotmail.com",
            'password' => bcrypt('123456'),
            'ciudad_id' => $ciudads_creados->random()->id
        ],
            [
            'nombre' =>"Juan",
            'email' =>"juan.fuentes.q@correosaiep.cl",
            'password' => bcrypt("123456"),
            'ciudad_id' => $ciudads_creados->random()->id
            ],
            [
            'nombre' =>"Mirko",
            'email' =>"mirko2302@hotmail.com",
            'password' => bcrypt("123456"),
            'ciudad_id' => $ciudads_creados->random()->id
            ],
             [
            'nombre' =>"Joaquín",
            'email' =>"joako9993@gmail.com",
            'password' => bcrypt("123456"),
            'ciudad_id' => $ciudads_creados->random()->id
            ],
              [
            'nombre' =>"Bastían",
            'email' =>"blate.whick@gmail.com",
            'password' => bcrypt("123456"),
            'ciudad_id' => $ciudads_creados->random()->id
            ]


        ];

        for ($i=0; $i < count($usuarios) ; $i++) {

          User::create($usuarios[$i]);
        };
    }
    
}
