<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Pais;
use App\Ciudad;
use App\Libro;
use App\Editorial;
use App\Autor;


class EditorialesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
             DB::table('editorials')->delete();

        $today = date('Y-m-d H:i:s');

        $data = [
            ['nombre' => 'ALienta'],
            ['nombre' => 'Austral'],
            ['nombre' => 'Backlist'],
            ['nombre' => 'Booket'],
            ['nombre' => 'Click'],
            ['nombre' => 'Destino'],
            ['nombre' => 'Deusto'],
            ['nombre' => 'Corin Tellado'],
            ['nombre' => 'Luciernaga'],
            ['nombre' => 'MR'],
            ['nombre' => 'Oniro'],
            ['nombre' => 'Península'],
            ['nombre' => 'Paidós'],
            ['nombre' => 'Temas de Hoy'],
            ['nombre' => 'Ariel'],
            ['nombre' => 'Crítica'],
            ['nombre' => 'Planeta'],
            ['nombre' => 'Zafiro'],
            ['nombre' => 'TusQuets'],
            ['nombre' => 'Timunmas'],
            ['nombre' => 'Diana'],
            ['nombre' => 'Esencia'],
            ['nombre' => 'Espasa'],
            ['nombre' => 'Minotauro'],
              ['nombre' => 'Noguer'],
            ['nombre' => 'Dummies'],
            ['nombre' => 'Lunwerg'],
            ['nombre' => 'Timun Mas Narrativa']
                        
        ];

        foreach ($data as $editorial) {
            DB::table('editorials')->insert([
                'nombre' => $editorial['nombre'],
                'created_at' => $today,
                'updated_at' => $today
            ]);


    	}
	
	}
}