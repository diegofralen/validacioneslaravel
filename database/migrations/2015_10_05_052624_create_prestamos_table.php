<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestamosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestamos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_inicio');
            $table->date('fecha_entrega');
            $table->boolean('estado');
            $table->integer('libro_id')->unsigned()->nullable();
            $table->integer('socio_id')->unsigned()->nullable();
            $table->foreign('libro_id')->references('id')->on('libros');
            $table->foreign('socio_id')->references('id')->on('socios');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestamos');
    }
}
