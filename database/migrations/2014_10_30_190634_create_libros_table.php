<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('isbn')->unique();
            $table->string('titulo')->unique();
            $table->string('edicion');
            $table->integer('numero_de_paginas');
            $table->year('año_edicion');
            $table->integer('numero_copias');
            $table->tinyinteger('estado');
            $table->integer('autor_id')->unsigned()->nullable();
            $table->integer('editorial_id')->unsigned()->nullable();
            $table->integer('categoria_id')->unsigned()->nullable();
            $table->integer('ciudad_id')->unsigned()->nullable();
            

            $table->foreign('autor_id')->references('id')->on('autors')->onDelete('cascade');
            $table->foreign('editorial_id')->references('id')->on('editorials')->onDelete('cascade');
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');
             $table->foreign('ciudad_id')->references('id')->on('ciudads')->onDelete('cascade');
           

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
          Schema::dropIfExists('libros');
    }
}
