<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libro;
use App\Editorial;
use App\Autor;
use App\Categoria;
use App\Pais;
use App\User;
use App\Prestamo;
use App\Ciudad;
use App\Socio;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Routing\Redirector;
use Carbon\Carbon;
use App\Http\Requests\StoreRegister;
use Alert;

class HomeController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    
    public function index()
    {
        $socios = Socio::all();
        return view('home', compact('socios'));
    }
    
    public function socio()
    {
        $ciudads = Ciudad::all();
        $libros = Libro::all();
        
        return view('socio', compact('ciudads','libros'));
    }
    
    public function ingreso()
    {
       $autors = Autor::all();
       $categorias = Categoria::all();
       $editorials = Editorial::all();
       $ciudads= Ciudad::all();
        return view('libro_ingreso', compact('autors','categorias','editorials','ciudads'));
    }
    
    public function autor()
    {
        $paises = Pais::all();   
        return view('autor', compact('paises'));
    }

    public function user()
    {
        $ciudads = Ciudad::all();
        return view('usuario', compact('ciudads'));
    }

     public function control()
    {
        $socios = Socio::all();
        $ciudads= Ciudad::all();
        return view('controlsocio', compact('socios','ciudads'));
    }
    public function prestamo3()
    {
        $libros=Libro::all();
        $socios=Socio::all();
        return view('prestamo_libro', compact('libros','socios'));
    }
    
    public function prestamolibro(Request $request)
    {
        $u=Socio::find($request->socios);
        if (!$u->puedepedirlibro()) {
        return back()->withErrors(['No puedes pedir mas de 2 libros', 'The Message']);                
      }
        $fecha_ini = Carbon::parse($request->fecha_inicio)->format('Y-m-d');
        $fecha_en = Carbon::parse($request->fecha_entrega)->format('Y-m-d');
       
        $prestamos = new Prestamo;
        $prestamos->fecha_inicio=$fecha_ini;
        $prestamos->fecha_entrega=$fecha_en;
        $prestamos->libro_id=$request->libros;
        $prestamos->socio_id=$request->socios;
        $prestamos->estado=1;

        $prestamos->save();

        Libro::where('id',$request->libros)->increment('numero_copias',-1);

        return back()->withErrors(['Se a Ingresado Correctamente', 'The Message']);
    }


    public function ingresol(Request $request)
    {

        $libro = new Libro;
        $libro->isbn=$request->isbn;
        $libro->titulo=$request->titulo;
        $libro->edicion=$request->edicion;
        $libro->numero_de_paginas=$request->pagina;
        $libro->año_edicion=$request->ano;
        $libro->numero_copias=$request->copias;
        $libro->autor_id=$request->autors;
        $libro->editorial_id=$request->editorials;
        $libro->categoria_id=$request->categorias;
        $libro->estado=1;
        $libro->ciudad_id=$request->ciudad;
        $libro->save();

        return back()->withErrors(['Se a Ingresado Correctamente', 'The Message']);
    }

    public function ingreso2(Request $request)
    {
        $fecha_nac = Carbon::parse($request->fecha_nacimiento)->format('Y-m-d');
        $autors = new Autor;
        $autors->nombre=$request->nombre;
        $autors->apellido=$request->apellido;
        $autors->fecha_nacimiento=$fecha_nac;
        $autors->pais_id=$request->paises;

        $autors->save();
        return back()->withErrors(['Se a Ingresado Correctamente', 'The Message']);       
    }
   
    public function ingreso_user(Request $request)
    {
        $users = new User;
        $users->nombre=$request->nombre;
        $users->email=$request->email;
        $users->password=bcrypt($request->password);
        $users->ciudad_id=$request->ciudad;
        $users->direccion=$request->direccion;
        $users->save();

        return back()->withErrors(['Se a Ingresado Correctamente', 'The Message']);
    }

    public function ingreso_socio(StoreRegister $request)
    {
      
        $socios = new Socio;
        $socios->primer_nombre=$request->primer_nombre;
        $socios->segundo_nombre=$request->segundo_nombre;
        $socios->apellido_materno=$request->apellido_materno;
        $socios->apellido_paterno=$request->apellido_paterno;
        $socios->fecha_nacimiento = $request->fecha_nacimiento;
        $socios->ciudad_id=$request->ciudads;
        $socios->direccion=$request->direccion;
        $socios->telefono=$request->telefono;
        $socios->rut=$request->rut;

        $socios->save();

        return back()->withErrors(['Se a Ingresado Correctamente', 'The Message'] );
    }

    public function buscar(Request $request)
    {
        if ($request->busca !== null) {
            $buscador = Libro::with(['categoria', 'autor','editorial','ciudad'])->where('titulo',"like","%".$request->busca."%")->get();
        }else{
            $buscador = Libro::with(['categoria', 'autor','editorial','ciudad'])->get();
        }
        
        return view('busqueda',compact('buscador'));
    }
    
    public function buscarp(Request $request)
    {
        if ($request->buscar !== null) {
            $id_usuario = $request->buscar;
            $buscador = Prestamo::with(['libro.categoria','socio','libro.ciudad'])->whereHas('socio', function ($q2) use ($id_usuario) {
                $q2->where('id', $id_usuario);
            })->get();
        } else {
            $buscador = Prestamo::with(['libro.categoria','socio','libro.ciudad'])->get();
        }
        $socios = Socio::all();
        $ciudads= Ciudad::all();
        $editorials =  Editorial::all();
        //dd($buscador->toArray());
        return view('busquedaprestamo', compact('buscador','socios','ciudad','libro.ciudad'));
    }
    public function cerrar(Request $request)
    {
     Prestamo::where('id', $request->idCerrar)->update(array('estado'=>0));
        return back()->withErrors(['Prestamo cerrado correctamente', 'The Message']);
    }

   
   public function buscarsocio(Request $request)
   {
        if ($request->buscar !== null) {
            $id_usuario = $request->buscar;
            $buscador = Socio::with(['ciudad'])->whereHas('socio', function ($q2) use ($id_usuario) {
                $q2->where('id', $id_usuario);
            })->get();
        } else {
            $buscador = Socio::with(['ciudad'])->get();
        }
        $socios = Socio::all();
        $ciudads= Ciudad::all();
        $editorials =  Editorial::all();
        //dd($buscador->toArray());
        return view('controlsocio', compact('buscador','socios','ciudad'));
    }

    public function destroy(Request $request)

    {
     Socio::where('id', $request->idEliminar)->delete();
 
  return back()->withErrors(['Prestamo cerrado correctamente', 'The Message']);   

    }
}