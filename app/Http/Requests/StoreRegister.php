<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\Rut;
use App\Rules\Phone;

class StoreRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rut' => ['required', 'string', new Rut],
             'primer_nombre' =>['required','alpha'],
            'segundo_nombre' => 'required|string|min:3|max:150',
            'apellido_materno' => 'required|alpha|min:3|max:150',
            'apellido_paterno' => 'required|alpha|min:3|max:150',
            'direccion' => 'required|string|min:3|max:150',
            'telefono' => ['required','string',new Phone],
            'ciudads' => 'required|integer|exists:ciudads,id',
            'fecha_nacimiento' => 'required|date',
        ];
    }
}
