<?php

namespace App;
use App\User;
use App\Pais;
use App\Ciudad;
use App\Editorial;
use App\Autor;
use App\Libro;
use App\Categoria;
use App\Prestamo;
use App\Socio;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    protected  $table='libros';
  
    public function editorial ()
    {
        return $this->belongsTo('App\Editorial');
    }
     public function autor ()
    {
        return $this->belongsTo('App\Autor');
    }
     public function categoria ()
    {
        return $this->belongsTo('App\Categoria');
    }
     public function prestamos ()
    {
        return $this->hasMany('App\Prestamo');
    }
     public function ciudad ()
    {
        return $this->belongsTo('App\Ciudad');
    }
}
