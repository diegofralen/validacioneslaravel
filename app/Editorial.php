<?php

namespace App;
use App\User;
use App\Pais;
use App\Ciudad;
use App\Editorial;
use App\Autor;
Use App\Libro;
use App\Categoria;
use App\Prestamo;
use App\Socio;
use Illuminate\Database\Eloquent\Model;

class Editorial extends Model
{
	protected $table='editorials';
	
    public function libro()
    {
        return $this->hasMany('App\Libro');
    }
}

