<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Nombre implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->validateNombre($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Nombre Tiene que ser con la primera letra Mayuscula';
    }

    private function validateNombre($value)
        {
          $Value = ucwords(strtolower($value));

        }
}
