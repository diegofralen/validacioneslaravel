<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Pais;
use App\Ciudad;
use App\tipolibro;
use App\Editorial;
use App\Autor;
use App\Libro;
use App\Categoria;
use App\Prestamo;
use App\Socio;

class Pais extends Model
{
	protected  $table='paises';

     public function autors ()
   
    {
        return $this->hasMany('App\Autor');
    }

}
