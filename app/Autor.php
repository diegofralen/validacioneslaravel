<?php

namespace App;
use App\User;
use App\Pais;
use App\Ciudad;
use App\Editorial;
use App\Autor;
use App\Libro;
use App\Prestamo;
use App\Socio;
use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
	protected  $table='autors';
	
     public function libros ()
    {
        return $this->hasMany('App\Libro');
    }
    public function pais ()
    {
     return $this->belongsTo('App\Pais');
    }

}
