<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Pais;
use App\Ciudad;
use App\tipolibro;
use App\Editorial;
use App\Autor;
use App\Libro;
use App\Categoria;
use App\Prestamo;
use App\Socio;

class Socio extends Model
{
   protected  $table='socios';

     public function prestamos ()
    {
    	return $this->hasMany('App\Prestamo');
    }

   public function ciudad ()
    {
       return $this->belongsTo('App\Ciudad');
    }

    public function puedepedirlibro()

    {
        return $this->prestamos()->count()<2;
    }
}
