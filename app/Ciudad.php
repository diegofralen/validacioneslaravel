<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Pais;
use App\Ciudad;
use App\Editorial;
use App\Autor;
use App\Libro;
use App\Categoria;
use App\Prestamo;
use App\Socio;
class Ciudad extends Model
{
  protected  $table='ciudads';
  
   public function users()
    {
        return $this->hasMany('App\User');
    }

     public function socios()
    {
        return $this->hasMany('App\Socio');
    }

      public function libro()
    {
        return $this->hasMany('App\Libro');
    }
}
