<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\User;
use App\Pais;
use App\Ciudad;
use App\tipolibro;
use App\Editorial;
use App\Autor;
use App\Libro;
use App\Categoria;
use App\Prestamo;
use App\Socio;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
     //public function ciudad ()
    //{
      //  return $this->belongsTo('App\Ciudad');
    //}
    

    // public function Libro ()
    //{
      //  return $this->hasMany('App\Libro');
    //}
}
