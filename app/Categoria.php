<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Pais;
use App\Ciudad;
use App\tipolibro;
use App\Editorial;
use App\Autor;
use App\Libro;
use App\Prestamo;
use App\Socio;
class Categoria extends Model
{
	protected  $table='categorias';

       public function libro ()
    {
        return $this->belongsTo('App\Libro');
    }
}
