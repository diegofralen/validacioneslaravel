<?php

namespace App\Helpers;

class RutFormatter
{
    public function formatRut($value)
    {
        $patterns = ["/\./", "/^0+(?!$)/"];

        if (strpos($value, "-") == false) {
            $pos = strlen($value) - 1;
            $value = substr_replace($value, "-", $pos) . substr($value, $pos);
        }

        return preg_replace($patterns, "", $value);
    }

    public function displayFormattedRut($value)
    {
        $patterns = ["/^0+(?!$)/"];

        if (strpos($value, "-") == false) {
            $pos = strlen($value) - 1;
            $value = substr_replace($value, "-", $pos) . substr($value, $pos);
        }
        $dv = substr($value, -1);

        $formatted = strpos($value, ".") ?
            $value :
            number_format(substr($value, 0, -2), 0, "", ".") . '-' . $dv;

        return preg_replace($patterns, "", $formatted);
    }
}
