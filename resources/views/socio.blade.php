<html lang="{{ app()->getLocale() }}">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/diseno_socio.css') }} "></link>

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"> 
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <div class="contenido">
        <div id="contenedor">
            <form action="{{url('/ingreso_socio')}}" method="post">
                @csrf
            <h1 class="title" style="text-align: center">Ingreso Socio</h1>
            <img style=""src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-vK1bqXbCwi93er0lZQVlUF-Yo2pRf41j8t2pxQZNqymknxAO" class="avatar">
            <label style=" width:100%; float:right; color:#609CF9; text-align:center; font-weight:bolder;" >
                
            </label>
            <p>Rut</p>
         
    <input onchange="puntoguion()" id="rut" type="text" class="form-control @if($errors->has('rut')) is-invalid @endif " placeholder="Rut *" name="rut" required="rut" value=""  />
              @if($errors->any())
                    {{$errors->first('rut')}}
                @endif
              
            <!--<input id="rut" type="text" style="width: 280px;"  name="rut1" placeholder="Ingrese Rut" required value="" />&nbsp -
            <input id="rut" type="text" style="width: 30px;" name="rut2" placeholder="K" maxlength="1" required value="" />-->
          <small id="rutHelp" class="form-text text-muted">ejemplo 22.717.721-7</small>
            <p>Nombres</p>
            <input type="text" style="width: 49%;" name="primer_nombre" placeholder="Primer Nombre" >
            <input type="text" style="width: 49%; float:right;" name="segundo_nombre" placeholder="Segundo Nombre" >
            <!--Nombre: required="required" 
            pattern="[A-Z]([a-z]){2,50}" title="solo letras, la primera mayuscula y sin espacios, si su nombre tiene una 'Ñ' remplacela por una 'N'"-->
           @if($errors->any())
                    {{$errors->first('primer_nombre')}}
                @endif
            <p>Apellidos</p>
            <input type="text" style="width: 49%;" name="apellido_paterno" placeholder="Primer Apellido" >
            @if($errors->any())
                    {{$errors->first('apellido_paterno')}}
                @endif
            <input type="text" style="width: 49%;float:right;"name="apellido_materno"placeholder="Segundo Apellido"> 
            @if($errors->any())
                    {{$errors->first('apellido_materno')}}
                @endif
            <p for="dob">Fecha de Nacimiento</p>
                <input type="date"  id="fecha_nacimiento" name="fecha_nacimiento" class="form-control @if($errors->has('fecha_nacimiento')) is-invalid @endif">
            <p>Telefono</p>
            <input type="tel" name="telefono" placeholder="9-XXXXXXX" >
            
            <!--telefono
            required="required" pattern="9([0-9]){8}" title="9 numeros y el primero tiene que ser el '9'"-->
             @if($errors->any())
                    {{$errors->first('telefono')}}
                @endif
            <p>Direccion</p>
                <select id="ciudads" class="form-control @if($errors->has('ciudads')) is-invalid @endif" placeholder=" *" name="ciudads"/>
                    <option disabled selected>Ciudades</option>
                    @foreach ($ciudads  as $ciudad)    
                        <option value="{{ $ciudad->id }}">{{ $ciudad->name }}</option>
                    @endforeach
                </select>
            <input type="text" name="direccion" placeholder="Direccion" style="margin-top: 15px;" >
            <input type="submit" class="btn btn-primary" style="margin-left:100px; margin-top: 20px;" value="Registrar Socio">
        </form>
        <footer>
            <h1 style="text-align: center; margin: 0px;">Biblioteck</h1>
            <form action="{{url('prestamo3')}}" style="margin: 0px;">
                <input type="submit" class="btn btn-success" style="float: right;" value="Siguiente">
            </form>
            <form action="{{url('home')}}" style="margin: 0px; float: left;">             
                <input  type="submit" class="btn btn-warning " value="Volver"/>
            </form> 
        </footer>
    </div>
</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>

<script>
    //formato punto y guion para rut  js 

function puntoguion(){
                var datos = $('#rut').val();
                var puntoguion = new String(datos);
 
                 if (puntoguion.length==8){
                    $('#rut').val(puntoguion.substring(0,1)+"."+puntoguion.substring(1,4)+"."+puntoguion.substring(4,7)+"-"+puntoguion.substring(7,8));
                }else{
                    if (puntoguion.length==9){
                        $('#rut').val(puntoguion.substring(0,2)+"."+puntoguion.substring(2,5)+"."+puntoguion.substring(5,8)+"-"+puntoguion.substring(8,9));
                    }
                }
            }

</script>

</body>
</html>