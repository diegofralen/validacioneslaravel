
<html lang="{{ app()->getLocale() }}">
    <title>Menu principal</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"> 
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/homediseno.css') }} "></link>
</head>
<body>
    <div id="contenedor">
        <div class="contenido">
            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card-body">
                    <h1 class="btn-lg" style="font-size: 18px; margin-left:10px; text-align: center;"><span class="glyphicon glyphicon-user"></span>Bienvenido {{auth()->user()->nombre}}</h1>
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2iJAkv0BdJ-e-3kfUq3K0ul5O2GL2jE8FBTLpA-_zVrU-yN64Fg" class="avatar">
                    <a href="{{url('usuario')}}"><input type="submit" name="usuario" class="btn btn-primary" value="Registrar Usuario"></a>
                    <a href="{{url('autor')}}"><input type="submit" name="libro" class="btn btn-success" value="Ingresar Libro"></a>
                    <a href="{{url('socio')}}"><input type="submit" name="prestamo" class="btn btn-success" value="Pedir Prestamo"></a>
                    <a href="{{url('buscar')}}"><input type="submit" name="infolibro" class="btn btn-info" value="Informacion Libro"></a>
                    <a href="{{url('buscarp')}}"><input type="submit" id="infoprestamo" class="btn btn-danger" value="Informacion de Prestamo"></a>
                     <a href="{{url('controlsocio')}}"><input type="submit" name="controlsocio" class="btn btn-dark" value="Control De Socios"></a>
                    <input  type="submit" style="width: 30%; float: right; margin-right: 35%;" name="volver" class="btn btn-warning"  href="{{ route('logout') }}" class="btn " value="Salir" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">                        
                        <form id="logout-form" action="{{ route('logout') }}"   method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    <footer>
                        <h1 style="text-align: center">Biblioteck</h1>
                    </footer>
                </div>
            </div>
        </div>
    </div>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

</body>
</html>