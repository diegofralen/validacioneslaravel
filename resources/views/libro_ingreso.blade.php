
<html lang="{{ app()->getLocale() }}">
<head>
    <title>Ingresar Libro</title>
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"> 
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<style>
    body{
    margin: 0;
    padding: 0;
    background: url(/img/libro.jpg);
    background-position: center;
    background-size: cover;
    background-attachment: fixed;
    background-repeat:no-repeat; 
}
.contenido{
    width: 550px;
    height: 670px;
    background-color: rgba(0, 0, 0, 0.5);
    color: #fff;
    top:50%;
    left: 50%;
    position: absolute;
    transform: translate(-50%, -50%);
    box-sizing: border-box;
    padding: 50px 30px 45px;
}
.avatar{
    width: 100px;
    height: 100px;
    top:-50px;
    left: calc(50% - 50px);
    position: absolute;
    border-radius: 50%;
    margin-bottom: 10px;
}
.title{
    margin: 0;
    padding: 0 0 10px;
    text-align: center;
    font-size: 22px;
}
.contenido input[type=text]{
    color: black;
    width: 100%;
    outline: none;
    height: 35px;
    font-size: 16px; 
    padding-left: 5px;
    border-radius: 10px;
}
.contenido input[type=number]{
    color: black;
    width: 100%;
    outline: none;
    height: 35px;
    font-size: 16px; 
    padding-left: 5px;
    border-radius: 10px;
}
.contenido p{
    font-size: 15px;
    width: 100%;
    font-weight: bold;
    margin-top: 10px;
    margin-bottom: 10px;
}
.contenido input[type=submit]{
    margin-top: 20px;
    margin-bottom: 0px;
}
.contenido select{
    color: black;
    width: 100%;
    outline: none;
    height: 35px;
    font-size: 16px; 
    padding-left: 5px;
    border-radius: 10px;
}
#parte1 {
    float: left;
    height: 400px;
    width: 50%;
    padding-right: 10px;
    padding-left: 10px;
}
#parte2 {
    float: right;
    height: 400px;
    width: 50%;
    padding-right: 10px;
    padding-left: 10px;
}

</style>
</head>
<body>
<div id="contenedor">
    <div class="contenido">
    <form action="{{url('ingresol')}}" method="post" style="margin-bottom: 0px;">
    @csrf

        <h1 class="title">Registrar libro</h1>
        <label style=" width:100%; float:right; color:#609CF9; text-align:center; font-weight:bolder;" >
            @if($errors->any())
                {{$errors->first()}}
            @endif
        </label>
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-vK1bqXbCwi93er0lZQVlUF-Yo2pRf41j8t2pxQZNqymknxAO" class="avatar">             
        <div id="parte1">
            <p>Ingrese Isbn</p>
                <input type="text" name="isbn" placeholder="ISBN" required="required" minlength="13" maxlength="24" pattern="[0-9]{13,24}" title="El ISBN deben ser SOLO NUMEROS y tener un largo de minimo 13 caracteres y maximo de 24">
            <p>Ingrese Titulo del Libro</p>
                <input type="text" name="titulo" placeholder="Ingrese Titulo" required="required">
            <p>Ingrese Edicion</p>
                <input type="text" name="edicion" placeholder="Ingrese tipo de edicion" required="required">
            <p>Ingrese Numero de Paginas</p>
                <input type="number" name="pagina" placeholder="Numeros de paginas" required="required" min="1">
            <p>Año de Edicion</p>
                <input type="number" class="form-control" value="2018" min="1000" max="2018" minlength="4" maxlength="4" name="ano" placeholder="Eje:1991" required="required" >
        </div>
        <div id="parte2">
            <p>Autor</p>
                <select id="autors" class="form-control @if($errors->has('autors')) is-invalid @endif" placeholder=" *" name="autors"/>
                    <option class="ocultar" selected disabled>Autores</option>
                    @foreach ($autors  as $autor)    
                        <option value="{{ $autor->id }}" requerid="requerid">{{ $autor->nombre }}</option>
                    @endforeach
                </select>
            <p>Editorial</p>
                <select id="editorials" placeholder="" name="editorials" required/>
                    <option class="ocultar" selected disabled>Editoriales</option>
                    @foreach ($editorials  as $editorial)
                        <option value="{{ $editorial->id }}" requerid="requerid">{{ $editorial->nombre }}</option>
                    @endforeach
                </select>
            <p>Categorias</p>
                <select id="categorias" class="form-control @if($errors->has('categorias')) is-invalid @endif" placeholder=" *" name="categorias" required value="" />                                 
                    <option class="ocultar" selected disabled>Categorias</option>
                    @foreach ($categorias  as $categoria)    
                        <option value="{{ $categoria->id }}">{{ $categoria->name }}</option>
                    @endforeach
                </select>
            <p>Numero de Copias</p>
                <input type="number" name="copias" placeholder="Ingrese Numero copias" required="required" min="0">
         <p>Ubicación / Sede </p>
                <select id="ciudads" class="form-control @if($errors->has('ciudads')) is-invalid @endif" placeholder=" *" name="ciudad" required value=""/>
                    <option  class="ocultar" selected disabled>Ciudades</option>
                    @foreach ($ciudads  as $ciudad)    
                        <option value="{{ $ciudad->id }}">{{ $ciudad->name }}</option>
                    @endforeach
                </select>


        </div>
            <input type="submit" class="btn btn-primary" style="margin-left:170px;" value="Registrar Libro">
    </form> 
        <footer>
            <form action="{{url('')}}" style="margin-bottom: 0px; ">  
                <input type="submit" class="btn btn-danger" style="float: right; margin-bottom:35px;" value="Volver a Home">
            </form>       
            <form action="{{url('autor')}}" style="margin-bottom: 0px; height: 15px; color: white" >             
                <input  type="submit" class="btn btn-success" value="Volver a Autor"/>
            </form>                    
            <h1 style="margin-left:130px; text-align: center;">Biblioteck</h1>
        </footer>
   </div>
</div>   
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</body>
</html>