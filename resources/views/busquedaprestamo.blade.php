<!DOCTYPE html>
<html>
<head>
	<title>Busqueda</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{{ asset('css/busqueda_diseno.css') }}"></link>
</head>
<body>
	<div class="busqueda" style="height: 783px;">
		<img src="http://iamable.ucm.es/IamAble/sites/default/files/busqueda.png" class="avatar" >
			<h1 class="title">Informacion y Cancelacion de Prestamos</h1>	
			 <label style=" width:100%; float:right; color:#609CF9; text-align:center; font-weight:bolder; font-size: 20px;" >
                @if($errors->any())
                    {{$errors->first()}}
                @endif
            </label>
			<a href="{{url('home')}}"><input type="submit" name="Volver" class="btn btn-warning" value="Volver"></a>
            <form id="form-prestamo" class="ocultar" action="{{url('buscarp')}}" method="get" style="margin: 0px; float: right;">
                <select style="width: 69%; height: 37px; float: left;" name="buscar" class="form-control @if($errors->has('socios')) is-invalid @endif">
                    <option value="">Todos</option>
                    @foreach ($socios as $socio)
            			<option value="{{$socio->id}}" @if(request()->buscar == $socio->id) selected @endif>
            				{{$socio->primer_nombre." ".$socio->apellido_paterno}}
            			</option>
                    @endforeach
                </select>
                <button style="width: 29%; float: right; padding-left: 7px;" type="submit" class="btn btn-primary">Buscar</button>
            </form>
		<div class="tablacontent">
			<table class="table table-striped table-dark" style="border-radius: 10px; ">
				<thead>
					<tr>
						<th>Rut</th>
						<th>Socio</th>
						<th>Libro</th>
						<th>Categoria</th>
						<th>Fecha Pedido</th>
						<th>Fecha entrega</th>
						<th>Estado</th>
						<th>Dirección</th>
						<th>Cancelar Prestamo</th>
					</tr>
				</thead>
				
				<tbody>
					@foreach ($buscador as $r)
					<tr>
						<td>{{$r->socio->rut}}</td>
						<td>{{$r->socio->primer_nombre." ".$r->socio->apellido_paterno}}</td>
						<td>{{$r->libro->titulo}}</td>
						<td>{{$r->libro->categoria->name}}</td>
						<td>{{$r->fecha_inicio}}</td>
						<td>{{$r->fecha_entrega}}</td>
						<td>
							@if(($r->estado)===1)
								Prestamo Activo
							@else
								Prestamo Cancelado
							@endif
						</td>
						<td>{{$r->socio->direccion.",".$r->socio->ciudad->name}}</td>
						<td>
							@if(($r->estado)===1)
								<form action="{{url('cerrar')}}">
									<input type="hidden" name="idCerrar" value="{{$r->id}}">
									<input type="submit" value="Cancelar" class="btn btn-danger">
								</form>
							@else
								<form action="{{url('cerrar')}}">
									<input type="hidden" name="idCerrar" value="{{$r->destroy($r->id)}}">
									<input type="submit" value="Cancelar" class="btn btn-danger" disabled="disabled">
								</form>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
				</div>
			</table>
		</form>
	</div>
</body>
</html>