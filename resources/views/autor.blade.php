<html lang="{{ app()->getLocale() }}">
<head>
    <title>Ingresar Autor</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"> 
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="{{ asset('css/diseno_autor.css') }} "></link>
</head>
<body>
    <div class="contenido">
        <div id="contenedor">
            <h1 class="title">Registrar Autor</h1>
        <form action="{{route('ingresodos')}}" method="post">
            @csrf 
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-vK1bqXbCwi93er0lZQVlUF-Yo2pRf41j8t2pxQZNqymknxAO" class="avatar">
            <label style=" width:100%; float:right; color:#609CF9; text-align:center; font-weight:bolder;" >
                @if($errors->any())
                    {{$errors->first()}}
                @endif
            </label>
            <p>Ingrese Nombre</p>
            <input type="text" name="nombre" placeholder="Ingrese Nombre">
            <p>Ingrese Apellido</p>
            <input type="text" name="apellido" placeholder="Ingrese Apellido" >
            <p for="dob">Fecha de Nacimiento</p>
            <input  type="date"  id="fecha_nacimiento" name="fecha_nacimiento" class="form-control @if($errors->has('fecha_nacimiento')) is-invalid @endif" required>
            <p>Pais</p>
            <select id="paises" class="form-control @if($errors->has('paises')) is-invalid @endif" placeholder=" *" name="paises" required value="" />
                <option disabled selected>Paises</option>
                @foreach ($paises  as $pais)    
                    <option value="{{ $pais->id }}">{{ $pais->name }}</option>
                @endforeach
            </select>
            <input type="submit" class="btn btn-primary" style="margin-left:60px;" value="Registrar Autor">
        </form> 
        <footer>
            <form action="{{url('ingreso')}}">  
                <input type="submit" class="btn btn-success" style="float: right;" value="Siguiente">
            </form>
            <form action="{{url('home')}}">             
                <input  type="submit" class="btn btn-warning" style="float: left;" value="Cancelar"/>
            </form>
            <h1 style="text-align: center; margin-top: -20px;">Biblioteck</h1>
        </footer>
    </div>
</div> 

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</body>
</html>