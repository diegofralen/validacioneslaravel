<html lang="{{ app()->getLocale() }}">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/diseno_prestamo.css') }} "></link>

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"> 
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

</head>
<body> 
    <div class="contenido">
        <div id="contenedor">
            <form action="{{url('/prestamolibro')}}" method="post">
                @csrf
            <img  style=""src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-vK1bqXbCwi93er0lZQVlUF-Yo2pRf41j8t2pxQZNqymknxAO" class="avatar">
            <h1 class="title" style="text-align: center">Ingresar un Prestamo</h1>
            <label style=" width:100%; float:right; color:#609CF9; text-align:center; font-weight:bolder;" >
                @if($errors->any())
                    {{$errors->first()}}
                @endif
            </label>
            <p for="dob">Fecha De Inicio</p>
            <input  type="date"  id="fecha_inicio" name="fecha_inicio" class="form-control @if($errors->has('fecha_inicio')) is-invalid @endif" required>
            <p for="dob">Fecha De Entrega</p>
            <input  type="date"  id="fecha_entrega" name="fecha_entrega" class="form-control @if($errors->has('fecha_entrega')) is-invalid @endif" required>
            <p>Libro</p>
            <select id="libros" class="form-control @if($errors->has('libros')) is-invalid @endif" placeholder=" *" name="libros" required value=""/>
                @foreach ($libros  as $libro)    
                    <option value="{{ $libro->id }}">{{ $libro->titulo }}</option>
                @endforeach
            </select>
            <p>Socio</p>
                <select  id="socios" class="form-control @if($errors->has('socios')) is-invalid @endif" placeholder=" *" name="socios" required value="">
                 @foreach ($socios  as $socio)    
                    <option value="{{ $socio->id }}">{{ $socio->primer_nombre.' '.$socio->apellido_paterno.' ' .$socio->apellido_materno }}</option>
                @endforeach
                </select>
            <input type="submit" class="btn btn-primary" style="margin-left:40px;" value="Registrar Prestamo">
        </form>
            <footer>
                <h1 style="text-align: center; margin-bottom: 0px;">Biblioteck</h1>
            </footer>
        </div>
        <form action="{{url('home')}}" style="margin: 0px; float: right;">  
            <input type="submit" class="btn btn-danger" value="Home">
        </form>
        <form action="{{url('socio')}}" style="margin: 0px;">             
            <input  type="submit" class="btn btn-warning " value="Volver"/>
        </form> 
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</body>
</html>