<html>
<title>Ingresar Usuario</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"> 
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="{{ asset('css/diseno_user.css')}}"></link>
</head>
<body> 
<div class="contenido">
    <div id="contenedor">
        <form action="{{url('ingreso_user')}}" method="post">
            @csrf
            <h1 class="title">Registrar Usuario</h1>
            <label style=" width:45%; float:right; color:#609CF9; text-align:center; font-weight:bolder;" >
                @if($errors->any())
                    {{$errors->first()}}
                @endif
            </label>
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2iJAkv0BdJ-e-3kfUq3K0ul5O2GL2jE8FBTLpA-_zVrU-yN64Fg" class="avatar">
            <p>Nombre</p>
            <input type="text" name="nombre" placeholder="Ingrese Nombre" required="required" pattern="[A-Z]([a-z]){2,50}" title="solo letras, la primera mayuscula y sin espacios, si su nombre tiene una 'Ñ' remplacela por una 'N'">
            <p>Email</p>
       	    <input type="email" name="email" placeholder="Ingrese Email" required="required">
            <p>Password</p>
            <input type="password" name="password" placeholder="Ingrese su Contraseña" required="required">
            <p>Ciudad</p>
            <select id="ciudads" class="form-control @if($errors->has('ciudads')) is-invalid @endif" placeholder=" *" name="ciudads" required value=""/>
                @foreach ($ciudads  as $ciudad)    
                    <option value="{{ $ciudad->id }}">{{ $ciudad->name }}</option>
                @endforeach
            </select>
            <p>Dirección</p>   
            <input type="text" name="direccion" placeholder="Ingrese Direccion" required="required" />
            <input type="submit" class="btn btn-success" style="margin-top:30px; color: white;" value="Registrar">    
        </form> 
        <footer>
            <form action="{{url('home')}}">             
               <input  type="submit" class="btn btn-danger" style=" color: white;" value="Cancelar"/>
            </form>
            <h1 style="text-align: center; margin-top: 5px;">Biblioteck</h1>
        </footer>  	
    </div>
</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

</body>
</html>