<!DOCTYPE html>
<html>
<head>
    <title>Busqueda</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/busqueda_diseno.css') }}"></link>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="busqueda" style="height: 783px;">
        <img src="http://iamable.ucm.es/IamAble/sites/default/files/busqueda.png" class="avatar" >
            <h1 class="title">Control Socios</h1>   
             <label style=" width:100%; float:right; color:#609CF9; text-align:center; font-weight:bolder; font-size: 20px;" >
                @if($errors->any())
                    {{$errors->first()}}
                @endif
            </label>
            <a href="{{url('home')}}"><input type="submit" name="Volver" class="btn btn-warning" value="Volver"></a>
            <form id="form-prestamo" class="ocultar" action="{{url('buscasocio')}}" method="get" style="margin: 0px; float: right;">
                <select style="width: 69%; height: 37px; float: left;" name="buscar" class="form-control @if($errors->has('socios')) is-invalid @endif">
                    <option value="">Todos</option>
                    @foreach ($socios as $socio)
                        <option value="{{$socio->id}}" @if(request()->buscar == $socio->id) selected @endif>
                            {{$socio->primer_nombre." ".$socio->apellido_paterno}}
                        </option>
                    @endforeach
                </select>
                <button style="width: 29%; float: right; padding-left: 7px;" type="submit" class="btn btn-primary">Buscar</button>
            </form>
        <div class="tablacontent">
            <table class="table table-striped table-dark" style="border-radius: 10px; ">
                <thead>
                    <tr>
                        <th style="text-align:center">Rut</th>
                        <th style="text-align:center">Socio</th>
                        <th style="text-align:center">Dirección</th>
                        <th style="text-align:center">Eliminar</th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach ($socios as $r)
                    <tr>
                        <td>{{$r->rut}}</td>
                        <td>{{$r->primer_nombre." ".$r->apellido_paterno}}</td>
                        <td>{{$r->direccion.",".$r->ciudad->name}}</td>
                        <td>
                    
        <form action="{{url('destroy')}}" method="get">
               <input name="idEliminar" type="hidden" value="{{$r->id}}">
             <button class="btn btn-danger " type="submit"><span class="glyphicon glyphicon-trash"></span></button>
        </form>
                         
                        
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                </div>
            </table>
        </form>
    </div>
</body>
</html>