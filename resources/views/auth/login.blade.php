<html>




<head>
    
    <title>Log In</title>

<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"> 
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />
     <link rel="stylesheet" href="{{ asset('css/diseno_login.css') }} "></link>
     <link rel="stylesheet" href="{{ asset('css/video.css')}}" type="text/css" />
    <link rel="stylesheet"
  href="https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css">
  <!-- or -->
  <link rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
</head>
<body>

         <header class="header content">
      <div class="header-video">
       <video autoplay muted loop   src="video/video.mp4">

      </div>

  <div class="header-overlay"></div>
      <div class="header-content">
       <div class="contenido animated zoomIn">
    <img src="https://png.pngtree.com/element_origin_min_pic/16/08/06/1957a5cd33007a0.jpg" class="avatar">
    <h1 class=" animated bounceInDown">Biblioteck</h1>
        
    
            
            <form  method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
            <p class="animated fadeIn">Email</p>
            <tr class="{{ $errors->has('email') ? ' has-error' : '' }}">
            <input type="email" name="email" value="{{ old('email') }}"  placeholder="Ingrese su Email" required autofocus required>
        </tr>
        <p class="animated fadeIn">Contrasena</p>
        <tr class="{{ $errors->has('password') ? ' has-error' : '' }}">
            <input style="" type="password" name="password"  placeholder="Ingrese Password" required="required">
        </tr>
        <input type="submit" class="btn btn-primary animated zoomIn">
          
                </tr>
            
           
            
            </form>
           
     @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
@if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif            
        <h5 style="margin-top: 90px; font-size: 10px; text-align: center;">Created for: Diego Ramos Rios, Joaquin Rojo</h5>
</div>
      </div>
    </header>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</body>
</html>