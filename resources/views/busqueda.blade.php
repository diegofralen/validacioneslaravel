<!DOCTYPE html>
<html>
<head>
	<title>Busqueda</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{{ asset('css/busqueda_diseno.css') }}"></link>
</head>
<body>
	<div class="busqueda" style="width: 1240px;">			
		<img src="http://iamable.ucm.es/IamAble/sites/default/files/busqueda.png" class="avatar" >
			<h1 class="title">Informacion Libros</h1>	
			<a href="{{url('home')}}"><input type="submit" name="Volver" class="btn btn-warning" value="Volver"></a>
			<form id="form-libro" style="margin: 0px; float: right;" action="{{url('buscar')}}" method="get">
			   <input style="width: 69%; height: 37px;" type="text" name="busca" placeholder="Buscar Nombre">
			   <button style="width: 29%; padding-left: 7px;" type="submit" class="btn btn-primary" name="buscar">Buscar</button>
			</form>
			<table class="table table-striped table-dark" style="border-radius: 10px; overflow-y: auto;">
				<thead>
					<tr>
						<th>Titulo</th>
						<th>Autor</th>
						<th>Categoria</th>
						<th>Numero de Paginas</th>
						<th>Edicion</th>
						<th>Editorial</th>
						<th>Año</th>
						<th>Copias Disponible</th>
						<th>Estado</th>
						<th>Ubicación/Sede</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($buscador as $r)
					<tr>
						<td>{{$r->titulo}}</td>
						<td>{{$r->autor->nombre." ".$r->autor->apellido}}</td>
						<td>{{$r->categoria->name}}</td>
						<td>{{$r->numero_de_paginas}}</td>
						<td>{{$r->edicion}}</td>
						<td>{{$r->editorial->nombre}}</td>
						<td>{{$r->año_edicion}}</td>
						<td>{{$r->numero_copias}}</td>
						<td>
							@if(($r->numero_copias)>0)	
								@if(($r->estado)===1)
									Disponible
								@else
									No Disponible
								@endif
							@else
								No Disponible
							@endif
						</td>
						<td>{{$r->ciudad->name}}</td>
			



					</tr>
					@endforeach
				</tbody>
			</table>
		</form>
	</div>
</body>
</html>