<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('login');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/socio', 'HomeController@socio')->name('socio');
Route::get('/ingreso', 'HomeController@ingreso')->name('libro_ingreso');
Route::get('/autor', 'HomeController@autor')->name('autor');
Route::get('/usuario', 'HomeController@user')->name('usuario');
Route::get('/controlsocio', 'HomeController@control')->name('ctrl');
Route::get('/buscar','HomeController@buscar')->name('busqueda');
Route::get('/prestamo3', 'HomeController@prestamo3')->name('prestamo_libro');
Route::get('/buscarp', 'HomeController@buscarp')->name('busquedaprestamo');
Route::get('/controlsocio', 'HomeController@buscarsocio')->name('buscasocio');
Route::get('/cerrar', 'HomeController@cerrar')->name('cerrar');

Route::get('/destroy', 'HomeController@destroy')->name('destroy');

Route::post('/ingresol', 'HomeController@ingresol');
Route::post('/ingreso2', 'HomeController@ingreso2')->name('ingresodos');
Route::post('/ingreso_user', 'HomeController@ingreso_user');
Route::post('/ingreso_socio', 'HomeController@ingreso_socio')->name('socios');
Route::post('/prestamolibro', 'HomeController@prestamolibro')->name('prestamolibro');